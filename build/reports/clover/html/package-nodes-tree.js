 

var Packages = {
    nodes: [
                                                                                                        
                
{
    "id": "com.epam.ld.module2.testing",
    "text": "com.epam.ld.module2.testing",
    "package": "com.epam.ld.module2.testing",
    "url": "com/epam/ld/module2/testing/pkg-summary.html",
            "coverage": "35.4%",
        "icon": "aui-icon aui-icon-small aui-iconfont-devtools-folder-closed",
            "li_attr": {"data-is-link": "true"},
        "a_attr": {"href": "com/epam/ld/module2/testing/pkg-summary.html"},
    "children": [
                                            
                
{
    "id": "com.epam.ld.module2.testing.annotation",
    "text": "annotation",
    "package": "com.epam.ld.module2.testing.annotation",
    "url": "com/epam/ld/module2/testing/annotation/pkg-summary.html",
            "coverage": " - ",
        "icon": "aui-icon aui-icon-small aui-iconfont-devtools-folder-closed",
            "li_attr": {"data-is-link": "true"},
        "a_attr": {"href": "com/epam/ld/module2/testing/annotation/pkg-summary.html"},
    "children": [
                    ]
},
                                    
                
{
    "id": "com.epam.ld.module2.testing.output",
    "text": "output",
    "package": "com.epam.ld.module2.testing.output",
    "url": "com/epam/ld/module2/testing/output/pkg-summary.html",
            "coverage": "0%",
        "icon": "aui-icon aui-icon-small aui-iconfont-devtools-folder-closed",
            "li_attr": {"data-is-link": "true"},
        "a_attr": {"href": "com/epam/ld/module2/testing/output/pkg-summary.html"},
    "children": [
                    ]
},
                                    
                
{
    "id": "com.epam.ld.module2.testing.resolve",
    "text": "resolve",
    "package": "com.epam.ld.module2.testing.resolve",
    "url": "com/epam/ld/module2/testing/resolve/pkg-summary.html",
            "coverage": "0%",
        "icon": "aui-icon aui-icon-small aui-iconfont-devtools-folder-closed",
            "li_attr": {"data-is-link": "true"},
        "a_attr": {"href": "com/epam/ld/module2/testing/resolve/pkg-summary.html"},
    "children": [
                    ]
},
                                    
                
{
    "id": "com.epam.ld.module2.testing.service",
    "text": "service",
    "package": "com.epam.ld.module2.testing.service",
    "url": "com/epam/ld/module2/testing/service/pkg-summary.html",
            "coverage": "33.3%",
        "icon": "aui-icon aui-icon-small aui-iconfont-devtools-folder-closed",
            "li_attr": {"data-is-link": "true"},
        "a_attr": {"href": "com/epam/ld/module2/testing/service/pkg-summary.html"},
    "children": [
                    ]
},
                                    
                
{
    "id": "com.epam.ld.module2.testing.template",
    "text": "template",
    "package": "com.epam.ld.module2.testing.template",
    "url": "com/epam/ld/module2/testing/template/pkg-summary.html",
            "coverage": "100%",
        "icon": "aui-icon aui-icon-small aui-iconfont-devtools-folder-closed",
            "li_attr": {"data-is-link": "true"},
        "a_attr": {"href": "com/epam/ld/module2/testing/template/pkg-summary.html"},
    "children": [
                    ]
},
            ]
},
            ],
    settings: {
        "icons": {
            "package": {
                "open": "aui-icon aui-icon-small aui-iconfont-devtools-folder-open",
                "closed": "aui-icon aui-icon-small aui-iconfont-devtools-folder-closed"
            },
            "state": {
                "collapsed": "aui-icon aui-icon-small aui-iconfont-collapsed",
                "expanded": "aui-icon aui-icon-small aui-iconfont-expanded",
                "forRemoval": "hidden aui-iconfont-collapsed aui-iconfont-expanded"
            }
        }
    }
};
