var treeMapJson = {"id":"Clover database Wed Oct 20 2021 12:42:19 UZT0","name":"","data":{
    "$area":300.0,"$color":49.666668,"title":
    " 300 Elements, 49.7% Coverage"},"children":[{"id":
      "com.epam.ld.module2.testing.output0","name":
      "com.epam.ld.module2.testing.output","data":{"$area":4.0,"$color":
        0.0,"title":
        "com.epam.ld.module2.testing.output 4 Elements, 0% Coverage"},
      "children":[{"id":"OutputResolver0","name":"OutputResolver","data":{
            "$area":0.0,"$color":-100.0,"path":
            "com/epam/ld/module2/testing/output/OutputResolver.html#OutputResolver",
            "title":"OutputResolver 0 Elements,  -  Coverage"},"children":[]},
        {"id":"OutputResolverImpl0","name":"OutputResolverImpl","data":{
            "$area":4.0,"$color":0.0,"path":
            "com/epam/ld/module2/testing/output/OutputResolverImpl.html#OutputResolverImpl",
            "title":"OutputResolverImpl 4 Elements, 0% Coverage"},"children":
          []}]},{"id":"com.epam.ld.module2.testing4","name":
      "com.epam.ld.module2.testing","data":{"$area":48.0,"$color":
        35.416664,"title":
        "com.epam.ld.module2.testing 48 Elements, 35.4% Coverage"},
      "children":[{"id":"ApplicationMode4","name":"ApplicationMode","data":{
            "$area":7.0,"$color":0.0,"path":
            "com/epam/ld/module2/testing/ApplicationMode.html#ApplicationMode",
            "title":"ApplicationMode 7 Elements, 0% Coverage"},"children":[]},
        {"id":"Application11","name":"Application","data":{"$area":18.0,
            "$color":0.0,"path":
            "com/epam/ld/module2/testing/Application.html#Application",
            "title":"Application 18 Elements, 0% Coverage"},"children":[]},{
          "id":"MailServer29","name":"MailServer","data":{"$area":2.0,
            "$color":100.0,"path":
            "com/epam/ld/module2/testing/MailServer.html#MailServer","title":
            "MailServer 2 Elements, 100% Coverage"},"children":[]},{"id":
          "Client31","name":"Client","data":{"$area":4.0,"$color":100.0,
            "path":"com/epam/ld/module2/testing/Client.html#Client","title":
            "Client 4 Elements, 100% Coverage"},"children":[]},{"id":
          "Messenger35","name":"Messenger","data":{"$area":6.0,"$color":
            0.0,"path":
            "com/epam/ld/module2/testing/Messenger.html#Messenger","title":
            "Messenger 6 Elements, 0% Coverage"},"children":[]},{"id":
          "MailServerTest41","name":"MailServerTest","data":{"$area":11.0,
            "$color":100.0,"path":
            "com/epam/ld/module2/testing/MailServerTest.html#MailServerTest",
            "title":"MailServerTest 11 Elements, 100% Coverage"},"children":[]}]},
    {"id":"com.epam.ld.module2.testing.resolve52","name":
      "com.epam.ld.module2.testing.resolve","data":{"$area":32.0,"$color":
        0.0,"title":
        "com.epam.ld.module2.testing.resolve 32 Elements, 0% Coverage"},
      "children":[{"id":"ParameterResolver52","name":"ParameterResolver",
          "data":{"$area":0.0,"$color":-100.0,"path":
            "com/epam/ld/module2/testing/resolve/ParameterResolver.html#ParameterResolver",
            "title":"ParameterResolver 0 Elements,  -  Coverage"},"children":
          []},{"id":"ModeResolver52","name":"ModeResolver","data":{"$area":
            0.0,"$color":-100.0,"path":
            "com/epam/ld/module2/testing/resolve/ModeResolver.html#ModeResolver",
            "title":"ModeResolver 0 Elements,  -  Coverage"},"children":[]},{
          "id":"ModeResolverImpl52","name":"ModeResolverImpl","data":{
            "$area":28.0,"$color":0.0,"path":
            "com/epam/ld/module2/testing/resolve/ModeResolverImpl.html#ModeResolverImpl",
            "title":"ModeResolverImpl 28 Elements, 0% Coverage"},"children":[]},
        {"id":"ParameterResolverImpl80","name":"ParameterResolverImpl",
          "data":{"$area":4.0,"$color":0.0,"path":
            "com/epam/ld/module2/testing/resolve/ParameterResolverImpl.html#ParameterResolverImpl",
            "title":"ParameterResolverImpl 4 Elements, 0% Coverage"},
          "children":[]}]},{"id":"com.epam.ld.module2.testing.service84",
      "name":"com.epam.ld.module2.testing.service","data":{"$area":126.0,
        "$color":33.333336,"title":
        "com.epam.ld.module2.testing.service 126 Elements, 33.3% Coverage"},
      "children":[{"id":"ConsoleOutputManager84","name":
          "ConsoleOutputManager","data":{"$area":2.0,"$color":100.0,"path":
            "com/epam/ld/module2/testing/service/ConsoleOutputManager.html#ConsoleOutputManager",
            "title":"ConsoleOutputManager 2 Elements, 100% Coverage"},
          "children":[]},{"id":"OutputManager86","name":"OutputManager",
          "data":{"$area":0.0,"$color":-100.0,"path":
            "com/epam/ld/module2/testing/service/OutputManager.html#OutputManager",
            "title":"OutputManager 0 Elements,  -  Coverage"},"children":[]},
        {"id":"FileOutputManager86","name":"FileOutputManager","data":{
            "$area":16.0,"$color":0.0,"path":
            "com/epam/ld/module2/testing/service/FileOutputManager.html#FileOutputManager",
            "title":"FileOutputManager 16 Elements, 0% Coverage"},"children":
          []},{"id":"ConsoleInputManager102","name":"ConsoleInputManager",
          "data":{"$area":13.0,"$color":53.846157,"path":
            "com/epam/ld/module2/testing/service/ConsoleInputManager.html#ConsoleInputManager",
            "title":"ConsoleInputManager 13 Elements, 53.8% Coverage"},
          "children":[]},{"id":"InputManager115","name":"InputManager",
          "data":{"$area":0.0,"$color":-100.0,"path":
            "com/epam/ld/module2/testing/service/InputManager.html#InputManager",
            "title":"InputManager 0 Elements,  -  Coverage"},"children":[]},{
          "id":"FileInputManager115","name":"FileInputManager","data":{
            "$area":28.0,"$color":0.0,"path":
            "com/epam/ld/module2/testing/service/FileInputManager.html#FileInputManager",
            "title":"FileInputManager 28 Elements, 0% Coverage"},"children":[]},
        {"id":"FileInputManagerTest143","name":"FileInputManagerTest","data":
          {"$area":25.0,"$color":0.0,"path":
            "com/epam/ld/module2/testing/service/FileInputManagerTest.html#FileInputManagerTest",
            "title":"FileInputManagerTest 25 Elements, 0% Coverage"},
          "children":[]},{"id":"FileOutputManagerTest168","name":
          "FileOutputManagerTest","data":{"$area":9.0,"$color":0.0,"path":
            "com/epam/ld/module2/testing/service/FileOutputManagerTest.html#FileOutputManagerTest",
            "title":"FileOutputManagerTest 9 Elements, 0% Coverage"},
          "children":[]},{"id":"ConsoleOutputManagerTest177","name":
          "ConsoleOutputManagerTest","data":{"$area":0.0,"$color":-100.0,
            "path":
            "com/epam/ld/module2/testing/service/ConsoleOutputManagerTest.html#ConsoleOutputManagerTest",
            "title":"ConsoleOutputManagerTest 0 Elements,  -  Coverage"},
          "children":[]},{"id":"ConsoleOutputManagerTest.OutputResult177",
          "name":"ConsoleOutputManagerTest.OutputResult","data":{"$area":
            13.0,"$color":100.0,"path":
            "com/epam/ld/module2/testing/service/ConsoleOutputManagerTest.html#ConsoleOutputManagerTest.OutputResult",
            "title":
            "ConsoleOutputManagerTest.OutputResult 13 Elements, 100% Coverage"},
          "children":[]},{"id":"ConsoleInputManagerTest190","name":
          "ConsoleInputManagerTest","data":{"$area":20.0,"$color":100.0,
            "path":
            "com/epam/ld/module2/testing/service/ConsoleInputManagerTest.html#ConsoleInputManagerTest",
            "title":"ConsoleInputManagerTest 20 Elements, 100% Coverage"},
          "children":[]}]},{"id":"com.epam.ld.module2.testing.template210",
      "name":"com.epam.ld.module2.testing.template","data":{"$area":90.0,
        "$color":100.0,"title":
        "com.epam.ld.module2.testing.template 90 Elements, 100% Coverage"},
      "children":[{"id":"Template210","name":"Template","data":{"$area":
            24.0,"$color":100.0,"path":
            "com/epam/ld/module2/testing/template/Template.html#Template",
            "title":"Template 24 Elements, 100% Coverage"},"children":[]},{
          "id":"TemplateEngine234","name":"TemplateEngine","data":{"$area":
            12.0,"$color":100.0,"path":
            "com/epam/ld/module2/testing/template/TemplateEngine.html#TemplateEngine",
            "title":"TemplateEngine 12 Elements, 100% Coverage"},"children":[]},
        {"id":"TemplateTest246","name":"TemplateTest","data":{"$area":32.0,
            "$color":100.0,"path":
            "com/epam/ld/module2/testing/template/TemplateTest.html#TemplateTest",
            "title":"TemplateTest 32 Elements, 100% Coverage"},"children":[]},
        {"id":"TemplateEngineTest278","name":"TemplateEngineTest","data":{
            "$area":22.0,"$color":100.0,"path":
            "com/epam/ld/module2/testing/template/TemplateEngineTest.html#TemplateEngineTest",
            "title":"TemplateEngineTest 22 Elements, 100% Coverage"},
          "children":[]}]},{"id":
      "com.epam.ld.module2.testing.annotation300","name":
      "com.epam.ld.module2.testing.annotation","data":{"$area":0.0,"$color":
        -100.0,"title":
        "com.epam.ld.module2.testing.annotation 0 Elements,  -  Coverage"},
      "children":[{"id":"IntegrationTest300","name":"IntegrationTest","data":
          {"$area":0.0,"$color":-100.0,"path":
            "com/epam/ld/module2/testing/annotation/IntegrationTest.html#IntegrationTest",
            "title":"IntegrationTest 0 Elements,  -  Coverage"},"children":[]},
        {"id":"UnitTest300","name":"UnitTest","data":{"$area":0.0,"$color":
            -100.0,"path":
            "com/epam/ld/module2/testing/annotation/UnitTest.html#UnitTest",
            "title":"UnitTest 0 Elements,  -  Coverage"},"children":[]},{
          "id":"UnitParametrizedTest300","name":"UnitParametrizedTest",
          "data":{"$area":0.0,"$color":-100.0,"path":
            "com/epam/ld/module2/testing/annotation/UnitParametrizedTest.html#UnitParametrizedTest",
            "title":"UnitParametrizedTest 0 Elements,  -  Coverage"},
          "children":[]}]}]}
;
processTreeMapJson (treeMapJson);