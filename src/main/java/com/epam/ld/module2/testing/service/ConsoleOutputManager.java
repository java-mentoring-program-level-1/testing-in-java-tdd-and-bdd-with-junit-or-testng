package com.epam.ld.module2.testing.service;

/**
 * Console output manager class
 */
public class ConsoleOutputManager implements OutputManager {
    @Override
    public void output(String message) {
        System.out.println(message);
    }
}
