package com.epam.ld.module2.testing.service;

import java.io.IOException;

/**
 * Output manager interface
 */
public interface OutputManager {
    void output(String message) throws IOException;
}
