package com.epam.ld.module2.testing.service;

import java.io.IOException;
import java.util.Map;

/**
 * Input Manager Interface
 */
public interface InputManager {
    Map<String, String> input(String[] args) throws IOException;
}
