package com.epam.ld.module2.testing;

/**
 * Application mode enum
 */
public enum ApplicationMode {
    CONSOLE("subject", "receiver"),
    FILE("input", "output");

    private final String firstParam;
    private final String secondParam;

    ApplicationMode(String firstParam, String secondParam) {
        this.firstParam = firstParam;
        this.secondParam = secondParam;
    }

    public String getFirstParam() {
        return firstParam;
    }

    public String getSecondParam() {
        return secondParam;
    }
}
