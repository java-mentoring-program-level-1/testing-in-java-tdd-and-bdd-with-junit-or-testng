package com.epam.ld.module2.testing;

import com.epam.ld.module2.testing.output.OutputResolver;
import com.epam.ld.module2.testing.output.OutputResolverImpl;
import com.epam.ld.module2.testing.resolve.ModeResolver;
import com.epam.ld.module2.testing.resolve.ModeResolverImpl;
import com.epam.ld.module2.testing.resolve.ParameterResolver;
import com.epam.ld.module2.testing.resolve.ParameterResolverImpl;
import com.epam.ld.module2.testing.service.InputManager;
import com.epam.ld.module2.testing.service.OutputManager;
import com.epam.ld.module2.testing.template.Template;
import com.epam.ld.module2.testing.template.TemplateEngine;

import java.io.IOException;
import java.util.Map;

public class Application {
    /**
     * Running point of the application
     * @param args arguments
     */
    public static void main(String[] args) {
        ModeResolver modeResolver = new ModeResolverImpl();
        try {
            InputManager inputManager = modeResolver.resolveInputManager(args);
            ParameterResolver parameterResolver = new ParameterResolverImpl(inputManager);
            Map<String, String> parameters = parameterResolver.resolve(args);
            Client client = new Client();
            client.setAddresses("client@email.com");
            StringBuilder builderTemplateText = new StringBuilder("To:#{to} Subject:#{subject} From: "
                    + client.getAddresses() + "Message: " +
                    "Hello, I am new client. Could you explain the service workflow?");
            Template template = new Template(builderTemplateText, parameters);
            TemplateEngine templateEngine = new TemplateEngine();
            MailServer mailServer = new MailServer();
            Messenger messenger = new Messenger(mailServer, templateEngine);
            messenger.sendMessage(client, template);
            OutputManager outputManager = modeResolver.resolveOutputManger(args);
            OutputResolver outputResolver = new OutputResolverImpl(outputManager);
            outputResolver.output(templateEngine.generateMessage(template, client));
        } catch (IllegalArgumentException | IOException e) {
            e.printStackTrace();
        }
    }
}
