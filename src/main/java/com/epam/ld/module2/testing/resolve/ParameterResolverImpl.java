package com.epam.ld.module2.testing.resolve;

import com.epam.ld.module2.testing.service.InputManager;

import java.io.IOException;
import java.util.Map;

/**
 * Parameter Resolver Implementation
 */
public class ParameterResolverImpl implements ParameterResolver {
    private final InputManager manager;

    public ParameterResolverImpl(InputManager manager) {
        this.manager = manager;
    }

    @Override
    public Map<String, String> resolve(String[] args) throws IOException {
        return manager.input(args);
    }
}
