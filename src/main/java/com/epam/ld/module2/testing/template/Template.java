package com.epam.ld.module2.testing.template;

import com.epam.ld.module2.testing.Client;

import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * The type Template.
 */
public class Template {

    private final StringBuilder templateText;
    private final Pattern pattern = Pattern.compile("(#\\{\\w+})");
    private final Map<String, String> parameters;

    public Template(StringBuilder templateText, Map<String, String> parameters) {
        this.templateText = templateText;
        this.parameters = parameters;
    }

    /**
     * Check whether the passed parameters are compatible with template text
     * @return boolean
     */
    public boolean isReplaceable() {
        Set<String> foundPlaceholders = getPlaceholders();

        for (String placeholder : foundPlaceholders) {
            if (!parameters.containsKey(placeholder)) {
                return false;
            }
        }

        return true;
    }

    /**
     * retrieve all the placeholders from template text
     * @return set of placeholders
     */
    private Set<String> getPlaceholders() {
        Set<String> foundPlaceholders = new HashSet<>();

        Matcher matcher = pattern.matcher(templateText);
        while (matcher.find()) {
            String found = matcher.group(1);
            foundPlaceholders.add(found.substring(2, found.length() - 1));
        }

        return foundPlaceholders;
    }

    public StringBuilder getTemplateText() {
        return templateText;
    }

    public Map<String, String> getParameters() {
        return parameters;
    }
}
