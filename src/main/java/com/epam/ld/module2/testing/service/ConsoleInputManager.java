package com.epam.ld.module2.testing.service;

import java.util.HashMap;
import java.util.Map;

/**
 * Console input manager class
 */
public class ConsoleInputManager implements InputManager {
    @Override
    public Map<String, String> input(String[] args) {
        Map<String, String> parameters = new HashMap<>();
        for(String arg: args) {
            String[] tokens = arg.split("=");
            if(tokens.length != 2) {
                throw new IllegalArgumentException();
            }
            if(!tokens[0].equals("mode")) {
                parameters.put(tokens[0], tokens[1]);
            }
        }
        return parameters;
    }
}
