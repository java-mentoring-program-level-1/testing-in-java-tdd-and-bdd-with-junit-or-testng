package com.epam.ld.module2.testing.resolve;

import java.io.IOException;
import java.util.Map;

/**
 * Input Parameter Resolver Interface
 */
public interface ParameterResolver {
    Map<String, String> resolve(String[] args) throws IOException;
}
