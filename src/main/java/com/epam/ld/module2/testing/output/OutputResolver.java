package com.epam.ld.module2.testing.output;

import java.io.IOException;

/**
 * Output Resolver Interface
 */
public interface OutputResolver {
    void output(String message) throws IOException;
}
