package com.epam.ld.module2.testing.service;

import java.io.*;

/**
 * File Output Manager class
 */
public class FileOutputManager implements OutputManager {
    private File outputFile;

    /**
     * Constructor and fetches outputFile from args array
     * @param args array of arguments
     */
    public FileOutputManager(String[] args) {
        for(String arg: args) {
            String[] tokens = arg.split("=");
            if(tokens.length != 2) {
                throw new IllegalArgumentException();
            }
            if(tokens[0].equals("output")) {
                outputFile = new File(tokens[1]);
                break;
            }
        }
    }

    /**
     * Output message into file
     * @param message
     * @throws IOException when writing into file occurs errors
     */
    @Override
    public void output(String message) throws IOException {
        try(BufferedWriter bufferedWriter =
                    new BufferedWriter(new OutputStreamWriter(new FileOutputStream(outputFile)))) {
            bufferedWriter.write(message);
        }
    }
}
