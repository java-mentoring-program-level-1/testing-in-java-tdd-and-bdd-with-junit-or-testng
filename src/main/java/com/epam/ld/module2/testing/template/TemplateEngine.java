package com.epam.ld.module2.testing.template;

import com.epam.ld.module2.testing.Client;

import java.util.Map;

/**
 * The type Template engine.
 */
public class TemplateEngine {
    /**
     * Generate message string.
     *
     * @param template the template
     * @param client   the client
     * @return the string
     * @throws IllegalArgumentException when one of the parameters is null
     */
    public String generateMessage(Template template, Client client) throws IllegalArgumentException {
        return replacePlaceholders(template, client);
    }

    private String replacePlaceholders(Template template, Client client) throws IllegalArgumentException {
        StringBuilder templateText = template.getTemplateText();
        templateText.insert(0, "From: " + client.getAddresses() + ", ");

        if(!template.isReplaceable()) {
            throw new IllegalArgumentException("Template's subject or receiver address is null");
        }

        for(Map.Entry<String, String> entry: template.getParameters().entrySet()) {
            templateText.replace(0, templateText.length(),
                    templateText.toString().replaceAll("#\\{" + entry.getKey() + "}", entry.getValue()));
        }

        return templateText.toString();
    }
}
