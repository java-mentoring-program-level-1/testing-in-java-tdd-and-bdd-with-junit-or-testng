package com.epam.ld.module2.testing.output;

import com.epam.ld.module2.testing.service.OutputManager;

import java.io.IOException;

/**
 * Output Resolver Implementation
 */
public class OutputResolverImpl implements OutputResolver {
    private final OutputManager manager;

    public OutputResolverImpl(OutputManager manager) {
        this.manager = manager;
    }

    @Override
    public void output(String message) throws IOException {
        manager.output(message);
    }
}
