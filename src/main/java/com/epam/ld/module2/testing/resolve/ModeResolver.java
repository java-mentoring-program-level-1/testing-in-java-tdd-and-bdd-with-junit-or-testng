package com.epam.ld.module2.testing.resolve;

import com.epam.ld.module2.testing.service.InputManager;
import com.epam.ld.module2.testing.service.OutputManager;

/**
 * Mode Resolver Interface
 */
public interface ModeResolver {
    InputManager resolveInputManager(String[] args);
    OutputManager resolveOutputManger(String[] args);
}
