package com.epam.ld.module2.testing.service;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Map;

/**
 * File Input Manager class
 */
public class FileInputManager implements InputManager {
    /**
     * input parameters from args in array
     * @param args array of arguments
     * @return parameters
     * @throws IOException when reading file failure happens
     */
    @Override
    public Map<String, String> input(String[] args) throws IOException {
        String inputFile = null;
        for(String arg: args) {
            String[] tokens = arg.split("=");
            if(tokens.length != 2) {
                throw new IllegalArgumentException();
            }
            if(tokens[0].equals("input")) {
                inputFile = tokens[1];
                break;
            }
        }
        return readFile(inputFile);
    }

    /**
     * Read parameters from file
     * @param filePath file path
     * @return parameters
     * @throws IOException when reading file failure happens
     */
    public Map<String, String> readFile(String filePath) throws IOException {
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(new FileInputStream(filePath)));
        String line;
        Map<String, String> parameters = new HashMap<>();
        while ((line = bufferedReader.readLine())!=null) {
            String[] tokens = line.split("=");
            if(tokens.length != 2) {
                throw new IllegalArgumentException();
            }
            parameters.put(tokens[0], tokens[1]);
        }
        return parameters;
    }
}
