package com.epam.ld.module2.testing.resolve;

import com.epam.ld.module2.testing.service.*;

/**
 * Application Mode Resolver Implementation
 */
public class ModeResolverImpl implements ModeResolver {

    @Override
    public InputManager resolveInputManager(String[] args) {
        String[] modeTokens = getModeTokens(args);
        if(modeTokens.length != 2) {
            throw new IllegalArgumentException("Invalid mode parameter is provided");
        }
        if(modeTokens[1].equals("file")) {
            return new FileInputManager();
        }
        return new ConsoleInputManager();
    }

    @Override
    public OutputManager resolveOutputManger(String[] args) {
        String[] modeTokens = getModeTokens(args);
        if(modeTokens.length != 2) {
            throw new IllegalArgumentException("Invalid mode parameter is provided");
        }
        if(modeTokens[1].equals("file")) {
            return new FileOutputManager(args);
        }
        return new ConsoleOutputManager();
    }

    private String[] getModeTokens(String[] args) {
        if(args.length == 0 || !args[0].contains("mode")) {
            throw new IllegalArgumentException("Runtime parameters not provided");
        }
        return args[0].split("=");
    }

}
