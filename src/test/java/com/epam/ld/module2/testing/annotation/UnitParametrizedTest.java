package com.epam.ld.module2.testing.annotation;

import org.junit.jupiter.api.Tag;
import org.junit.jupiter.params.ParameterizedTest;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
@Unit
@Tag("UnitParametrizedTest")
@ParameterizedTest
public @interface UnitParametrizedTest {
}
