package com.epam.ld.module2.testing.service;

import com.epam.ld.module2.testing.Client;
import com.epam.ld.module2.testing.annotation.IntegrationTest;
import com.epam.ld.module2.testing.template.Template;
import com.epam.ld.module2.testing.template.TemplateEngine;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.PrintStream;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

class ConsoleOutputManagerTest {
    OutputManager manager;
    @Nested
    class OutputResult {
        private final ByteArrayOutputStream outResult = new ByteArrayOutputStream();
        private final PrintStream standardOut = System.out;

        @BeforeEach
        void setup() {
            System.setOut(new PrintStream(outResult));
        }

        @AfterEach
        void tearDown() {
            System.setOut(standardOut);
        }

        @IntegrationTest
        void checkOutputResultSuccess() throws IOException {
            manager = new ConsoleOutputManager();
            Template template = mock(Template.class);
            Client client = mock(Client.class);
            TemplateEngine engine = mock(TemplateEngine.class);
            String expectedMessage = "Subject: Greeting\n" +
                    "To: receiver@email.com\n" +
                    "From: client@email.com\n" +
                    "Message: Hello, nice to meet you!";
            when(engine.generateMessage(template, client)).thenReturn(expectedMessage);
            manager.output(engine.generateMessage(template, client));
            assertEquals(expectedMessage, outResult.toString().trim());
        }
    }
}