package com.epam.ld.module2.testing.service;

import com.epam.ld.module2.testing.Client;
import com.epam.ld.module2.testing.annotation.IntegrationTest;
import com.epam.ld.module2.testing.output.OutputResolver;
import com.epam.ld.module2.testing.output.OutputResolverImpl;
import com.epam.ld.module2.testing.template.Template;
import com.epam.ld.module2.testing.template.TemplateEngine;
import org.junit.jupiter.api.condition.EnabledIfEnvironmentVariable;

import java.io.IOException;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

class FileOutputManagerTest {
    OutputManager manager;
    @IntegrationTest
    @EnabledIfEnvironmentVariable(named = "mode", matches = "file")
    void outputMessageToFileSuccess() throws IOException {
        String[] args = {"input=D:\\EPAM\\IdeaProjects\\messenger\\src\\test\\resources\\input.txt",
                "output=D:\\EPAM\\IdeaProjects\\messenger\\src\\test\\resources\\output.txt"};
        manager = new FileOutputManager(args);
        Template template = mock(Template.class);
        Client client = mock(Client.class);
        TemplateEngine engine = mock(TemplateEngine.class);
        when(engine.generateMessage(template, client)).thenReturn("Hello, It is very helpful, thanks!");
        OutputResolver outputResolver = new OutputResolverImpl(manager);
        outputResolver.output(engine.generateMessage(template, client));
    }
}