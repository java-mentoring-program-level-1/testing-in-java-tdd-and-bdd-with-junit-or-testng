package com.epam.ld.module2.testing;

import com.epam.ld.module2.testing.annotation.UnitTest;
import com.epam.ld.module2.testing.template.TemplateEngine;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.io.PrintWriter;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.fail;

public class MailServerTest {
    private static final ByteArrayOutputStream outResult = new ByteArrayOutputStream();
    private static final PrintStream standardOut = System.out;

    @BeforeAll
    static void setup() {
        System.setOut(new PrintStream(outResult));
    }

    @AfterAll
    static void tearDown() {
        System.setOut(standardOut);
    }

    @UnitTest
    public void checkMessageOutSuccess() {
        MailServer mailServer = new MailServer();
        String message = "new message...";
        String addresses = "client@email.com";
        String expectedOutput = "Message: \"" + message + "\", successfully sent to \"" + addresses + "\"";
        mailServer.send(addresses, message);
        assertEquals(expectedOutput, outResult.toString().trim());
    }
}
