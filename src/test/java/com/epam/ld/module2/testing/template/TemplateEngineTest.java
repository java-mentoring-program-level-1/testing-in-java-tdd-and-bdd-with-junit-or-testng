package com.epam.ld.module2.testing.template;


import com.epam.ld.module2.testing.Client;
import com.epam.ld.module2.testing.annotation.UnitParametrizedTest;
import com.epam.ld.module2.testing.annotation.UnitTest;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.HashMap;
import java.util.Map;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.*;


public class TemplateEngineTest {

    static Stream<Arguments> testData() {
        Client client = new Client();
        client.setAddresses("client@email.com");
        return Stream.of(
                Arguments.of("To: #{to}, Subject: #{subject}", "From: " + client.getAddresses() + ", To: receiver@email.com, Subject: Greeting"),
                Arguments.of("To: {to}, Subject: #{subject}", "From: " + client.getAddresses() + ", To: {to}, Subject: Greeting"),
                Arguments.of("To: #{to}, Subject: #subject}", "From: " + client.getAddresses() + ", To: receiver@email.com, Subject: #subject}"),
                Arguments.of("To: to}, Subject: #{subject", "From: " + client.getAddresses() + ", To: to}, Subject: #{subject")
        );
    }

    @UnitParametrizedTest
    @MethodSource(value = {"testData"})
    void successTemplateTextGeneration(String templateText, String expectedText) {
        StringBuilder builderTemplateText = new StringBuilder(templateText);
        Map<String, String> parameters = new HashMap<>();
        parameters.put("to", "receiver@email.com");
        parameters.put("subject", "Greeting");
        Client client = new Client();
        client.setAddresses("client@email.com");
        Template template = new Template(builderTemplateText, parameters);
        TemplateEngine engine = new TemplateEngine();
        assertEquals(expectedText, engine.generateMessage(template, client));
    }

    @UnitTest
    void throwExceptionWhenGeneratingTemplateText() {
        StringBuilder builderTemplateText = new StringBuilder("To: #{to}, Subject: #{subject}");
        Map<String, String> parameters = new HashMap<>();
        Client client = new Client();
        client.setAddresses("client@email.com");
        Template template = new Template(builderTemplateText, parameters);
        TemplateEngine templateEngine = new TemplateEngine();
        assertThrows(IllegalArgumentException.class, ()->templateEngine.generateMessage(template, client));
    }
}
