package com.epam.ld.module2.testing.service;

import com.epam.ld.module2.testing.annotation.IntegrationTest;
import org.junit.jupiter.api.condition.EnabledIfEnvironmentVariable;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

class FileInputManagerTest {

    private FileInputManager manager;
    @IntegrationTest
    @EnabledIfEnvironmentVariable(named = "mode", matches = "file")
    void checkRetrieveParametersFromFileSuccess() throws IOException {
        String[] args = {"input=D:\\EPAM\\IdeaProjects\\messenger\\src\\test\\resources\\input.txt",
                "output=D:\\EPAM\\IdeaProjects\\messenger\\src\\test\\resources\\output.txt"};
        Map<String, String> parameters = new HashMap<>();
        parameters.put("subject", "Greeting");
        parameters.put("receiver", "receiver@email.com");
        manager = new FileInputManager();
        Map<String, String> expectedParameters = manager.input(args);

        assertAll(()-> {
            assertTrue(expectedParameters.containsKey("subject"));
            assertTrue(expectedParameters.containsKey("receiver"));
            assertEquals(parameters.get("subject"), expectedParameters.get("subject"));
            assertEquals(parameters.get("receiver"), expectedParameters.get("receiver"));
        });
    }

    @IntegrationTest
    @EnabledIfEnvironmentVariable(named = "mode", matches = "file")
    void throwExceptionWhenReadInvalidFile() throws IOException {
        String[] args = {"input=D:\\EPAM\\IdeaProjects\\messenger\\src\\test\\resources\\input1.txt",
                "output=D:\\EPAM\\IdeaProjects\\messenger\\src\\test\\resources\\output.txt"};
        manager = mock(FileInputManager.class);
        when(manager.input(args)).thenThrow(FileNotFoundException.class);

        assertThrows(FileNotFoundException.class, ()-> manager.input(args));
    }

    @IntegrationTest
    @EnabledIfEnvironmentVariable(named = "mode", matches = "file")
    void throwExceptionWhenReadInvalidParametersFromFile() throws IOException {
        String[] args = {"input=D:\\EPAM\\IdeaProjects\\messenger\\src\\test\\resources\\invalidParams.txt",
                "output=D:\\EPAM\\IdeaProjects\\messenger\\src\\test\\resources\\output.txt"};
        manager = new FileInputManager();
        assertThrows(IllegalArgumentException.class, ()-> manager.input(args));
    }

    @IntegrationTest
    @EnabledIfEnvironmentVariable(named = "mode", matches = "file")
    void throwExceptionWhenReadInvalidFileParameters() {
        String[] args = {"input=",
                "output=D:\\EPAM\\IdeaProjects\\messenger\\src\\test\\resources\\output.txt"};
        manager = new FileInputManager();
        assertThrows(IllegalArgumentException.class, ()-> manager.input(args));
    }
}