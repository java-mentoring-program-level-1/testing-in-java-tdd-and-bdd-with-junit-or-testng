package com.epam.ld.module2.testing.template;


import com.epam.ld.module2.testing.Client;
import com.epam.ld.module2.testing.annotation.UnitParametrizedTest;
import com.epam.ld.module2.testing.annotation.UnitTest;
import org.junit.jupiter.api.DynamicTest;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestFactory;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import org.junit.jupiter.params.provider.ValueSource;

import java.util.*;
import java.util.stream.IntStream;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.*;

public class TemplateTest {
    @TestFactory
    Stream<DynamicTest> dynamicTestsFromStream() {
        List<String> receivers = Arrays.asList("receiver1@email.com", "receiver2@email.com",
                "receiver3@email.com", "receiver4@email.com", "receiver5@email.com");
        List<String> subjects = Arrays.asList("Grooming", "Greeting",
                "Sync-up", "Weekly", "Monthly");
        return IntStream.range(0, 5).mapToObj(i -> DynamicTest.dynamicTest("checkParametersAreReplaceable: " + i,
                ()-> {
            Map<String, String> parameters = new HashMap<>();
            parameters.put("subject", subjects.get(i));
            parameters.put("to", receivers.get(i));
            Template template = new Template(new StringBuilder("To:#{to} Subject:#{subject}"), parameters);
            assertTrue(template.isReplaceable());
        }));
    }

    @UnitParametrizedTest
    @CsvSource(value =
            {"To:#{to} Subject:#{subject};receiver1@email.com;Greeting",
            "To:#{to} Subject:#{subject} Message:Hello, how are you?;receiver2@email.com;Grooming",
            "To:#{to} Subject:#{subject} Message:New Feature bugs?;receiver3@email.com;Sync-up"},
            delimiterString = ";")
    void parameterizedTestToCheckTemplateTextNotNull(String templateText, String receiverAddress, String subject) {
        System.out.println(templateText);
        System.out.println(receiverAddress);
        System.out.println(subject);
        Map<String, String> parameters = new HashMap<>();
        parameters.put("to", receiverAddress);
        parameters.put("subject",subject);
        Template template = new Template(new StringBuilder(templateText), parameters);
        assertTrue(template.isReplaceable());
    }


    @UnitTest
    void checkParametersAreReplaceable() {
        String text = "To: #{to}\n" +
                "Subject: #{subject}";
        Map<String, String> parameters = new HashMap<>();
        parameters.put("to","receiver@email.com");
        parameters.put("subject","Greeting");
        StringBuilder templateText = new StringBuilder(text);
        Template template = new Template(templateText, parameters);
        assertTrue(template.isReplaceable());
    }

    @UnitTest
    void checkParametersAreNotReplaceable() {
        String text = "To: #{to}\n" +
                "Subject: #{subject}";
        Map<String, String> parameters = new HashMap<>();
        StringBuilder templateText = new StringBuilder(text);
        Template template = new Template(templateText, parameters);
        assertFalse(template.isReplaceable());
    }
}