package com.epam.ld.module2.testing.service;

import com.epam.ld.module2.testing.annotation.IntegrationTest;
import com.epam.ld.module2.testing.annotation.UnitTest;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.spy;

class ConsoleInputManagerTest {
    InputManager manager;
    @IntegrationTest
    void checkRetrieveParametersFromConsoleSuccess() throws IOException {
        String[] args = {"key=value"};
        Map<String, String> parameters = new HashMap<>();
        parameters.put("key", "value");
        manager = spy(new ConsoleInputManager());
        doReturn(parameters).when(manager).input(args);

        Map<String, String> retrievedParameters = manager.input(args);
        assertEquals(parameters.get("key"), retrievedParameters.get("key"));
    }

    @IntegrationTest
    void checkRetrieveParametersFromConsoleFailure() throws IOException {
        String[] args = {"key=value"};
        Map<String, String> parameters = new HashMap<>();
        parameters.put("key", "value");
        manager = spy(new ConsoleInputManager());
        doReturn(parameters).when(manager).input(args);

        Map<String, String> retrievedParameters = manager.input(args);
        assertEquals(parameters.get("key"), retrievedParameters.get("key"));
    }

    @UnitTest
    void throwExceptionWhenReadParametersFromConsole() {
        String[] args = {"key=", "key1=value"};
        manager = new ConsoleInputManager();
        assertThrows(IllegalArgumentException.class, () -> manager.input(args));
    }
}